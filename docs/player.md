# Player

## State Machine
The state machine is derrived from [Game Endeavor's](https://www.youtube.com/channel/UCLweX1UtQjRjj7rs_0XQ2Eg) two videos ([1](https://youtu.be/BNU8xNRk_oU), [2](https://youtu.be/j_pM3CiQwts)) on state machines. I had previously tried "GDScript's" node based [state machine](https://gdscript.com/godot-state-machine) however it was overly complex and convoluted - it caused more problems than it solved.

## Jumping

### Math
The math for jumping is a mixture of a few different sources and my own changes. Matthew Rader has a good article on creating a better feeling [character controller](https://www.radermatthew.com/game-dev-blog/2d-platformer-controller-update). His work is done in unity, so velocity and gravity need to be inverted in Godot.

- [What are the kinematic formulas?](https://www.khanacademy.org/science/physics/one-dimensional-motion/kinematic-formulas/a/what-are-the-kinematic-formulas)
- [Physics in platformer games](https://2dengine.com/?p=platformers)
- [How to set jump height in Godot 3.0](https://youtu.be/918wFTru2-c)

### is_on_floor()
The `is_on_floor()` function will not work correctly if gravity is not being constantly applied. Do not turn off gravity when you are on the floor.

### Gravity
Gravity needs to be multiplied by delta. This is due to the fact that our gravity is time based - calculated from the time it takes to reach our jump apex.
