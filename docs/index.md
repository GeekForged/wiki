# Welcome to GeekForged

[Todo](todo.txt)

## Goals

- Create a world that is fun to be in and explore regardless of gameplay, enemies, etc.

## Development

### Notes

- [Player](player.md)

### Art/Visuals

- [Style](development/art/style.md)

### Gameplay

!!! tip
    - Rule #1 Keep things simple
    - Rule #2 Keep things balanced


- [Difficulty](development/gameplay/difficulty.md)
- [Enemies](development/gameplay/enemies.md)
- [Environments](development/gameplay/environments.md)
- [Health](development/gameplay/health.md)
- [Inventory](development/gameplay/inventory.md)
- [Save Station](development/gameplay/save_station.md)
- [Skills](development/gameplay/skills.md)
- [Weapons](development/gameplay/weapons.md)

### Storyline

There are two storylines to this game, the game world, which the player physically experiences and the real world that occasionally bleeds into the game world.

- [Main Boss](development/storyline/boss_(main).md)
- [Character](development/storyline/character.md)
- [Game world](development/storyline/game_world.md)
- [Outer world](development/storyline/outer_world.md)
