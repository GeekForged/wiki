# Weapons
Melee or ranged? No idea. Perhaps we could follow the simple path here and use a bit of a "standard" set of video game weapon types. It might not be wise to re-create the wheel here; there are a number of weapons that have already been proven to be fun to play with.

## Melee
- Hallagan bar
    - Drops down through the ice from the rescue team
- Axe
    - Same as Hallagan

## Ranged
This has some problems such as needing 8 directions to actually hit enemies.
- Throwing Knives
- Rainbow gun (some sort of rainbow projectile)

## Combination
- Chainsaw/Gun like the Lancer from Gears of War

## Disempowerment
- Shrink ray - makes big bad enemies not so big and bad
