# Save Station

Should anything be auto saved?

What should a save station look like?

- Telephone booth
    - Unsettling in the landscape
    - Foreign to most age groups
    - Could be used to tie back to the outer world story; the phone rings and you get a bit more of the outer world story each time. ~~For example the first time you come across it, you listen in on the 911 operator for your call, revisiting the booth does not replay the audio.~~ (we should not do this as it does not make sense for the character to hear this.)