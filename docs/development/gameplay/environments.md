# Environments

Assuming that there will be multiple sectioned environments, they will be as follows:



## Winter

Inspiration from the [Freezeezy Peak](https://banjokazooie.fandom.com/wiki/Freezeezy_Peak) level in Banjo-Kazooie. For some reason, [The Twinklies](https://banjokazooie.fandom.com/wiki/The_Twinklies) really left an impression on me as a kid. This Environment should have a variety of snow, ice, and holiday related enemies. Maybe, just maybe a Krampus appearance.



## Mystic Gardens

A smaller area, greenery, covered in fog and mist. The area has a mystic and puzzling aura about it. Players will find that even though they were headed in the right direction, they ended up somewhere else. Up is down, left is right. It's almost like this place is guarding something.