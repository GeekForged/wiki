# Enemies

## S'mores
A delightful snack turned evil - see sketchbook for thoughts

## Old lady
An old lady that drops hard candies to bait the player.

## Mechanics - handbook

- Projectiles **NEVER** pass through walls.
- Hordes should be used extremely sparingly as they can easily ruin gameplay.

## Winter

### Snow Gremlins

These creatures look like harmless snow men from afar; when approached they show their true nature. Each ball of the snowman is a gremlin, with razor sharp teeth, and stubby stick like arms and legs. They roll faster than they can run.
