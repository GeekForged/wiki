# Difficulty

## Bosses

Bosses should not be impossible to beat. For example, you should not have to fight a boss 30 times to just barely progress. If something like this is happening, the game is at fault. Here are some common situations, and how to overcome them:

- [Imbalanced character, not powerful enough to face the boss](#imbalanced-character)
- [Overly powerful boss](#overly-powerful-boss)
- [Overly confusing boss](#overly-confusing-boss)

### Imbalanced character

 There are multiple reasons a player could come upon a boss battle that they are not ready for, most of them can be attributed to poor level design. This fact is especially true in games where it is normal to block a players progression from an area if they are not meant to be there yet. I would like to aim for a more "open world". If a boss seems to be too hard, a player should have the option to bypass it and perhaps find a power-up to help with the boss.

!!! tip
    Pay attention to area access.

### Overly powerful boss

This is a relative issue, in some games, you have enough health to be a tank against a boss. In general, this is not a good solution, neither is relying on pure skill and reaction time. Relying on skill and reaction time has the unfortunate side effect of ruining the experience for casual players, as they are forced to sink a bunch of time into trying to defeat one boss over the course of many attempts.

Adaptive back-off can allow players who are having trouble with the boss to still enjoy the overall game.

!!! tip
    **Adaptive back-off:** A global variable that factors into the difficulty of (some) enemies. If a player has success in defeating these enemies, the back-off factor approaches 1.0. As a player is defeated by these enemies in succession (repeatedly) the back-off factor begins to approach 0.1. This factor could effect enemies properties such as: speed, health, damage, etc.

Bosses will need to be designed with care, and likely a bunch of play-testing from players with different skill levels (new and veteran video game players).

### Overly confusing boss

The player is unable to figure out how to weaken the boss, or doesn't understand it's patterns. A boss' weakness should be apparent to the player. It is somewhat common in games that used ranged weapons to require player to unload in different locations on a boss until they find the boss is being weakened. This is likely a failure in art direction and/or level design. When battling high level bosses, you shouldn't have to guess where to hit, as the battle is already designed to be difficult. This does not mean that a boss can't prevent you from hitting that area at times however.

!!! tip
    Boss' weaknesses should be apparent - both through art direction and level design. A boss probably shouldn't introduce entirely new weaknesses.
