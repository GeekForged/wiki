# Boss (main)

The main boss of the game is actually an "anti", "inverse", or "corrupted" version of the main character. This character represent depression, and malevolence, signifying that the main character must overcome herself to get better or become whole.



## Appearance

The main boss has pale skin with black eye sockets, [kind of like these](https://s3.amazonaws.com/coloring-queen/wp-content/uploads/2017/01/02133719/Untitled-design.png), or like iZombie [rage mode](https://i.ytimg.com/vi/u-mmvqCMLfM/maxresdefault.jpg).

