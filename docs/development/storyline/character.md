# Character

| Attributes |                           |
| ---------- | ------------------------- |
| Name       | Abigail (Aby)             |
| Gender     | Female                    |
| Ethnicity  | Greek / Mediterranean     |
| Hair color | Dark brown                |
| Skin color | Olive                     |
| Age        | 20s                       |
| Height     | 5' 6"                     |
| Residence  | Lower East Side Manhattan |

## Story

Our character's in game story can be found in [Storyline (game world)](game_world.md), there is also a "real world" story that coincides with our game timeline that can be found in [Storyline (outer world)](outer_world.md).

### Panic attacks

Our character is prone to panic attacks. these attacks come at some of the most unfortunate of times.

!!! missing
    We need to look up causes, triggers, etc.

!!! missing
    We need to describe some major life events prior to the game storyline.

## Likes

!!! missing
    What does our character like?


## Dislikes

!!! missing
    What does she dislike?
