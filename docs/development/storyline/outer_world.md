# Storyline (outer world)



## Overview

Aby rides her bike to the middle of the bridge climbs the fence and thrusts herself over, plummeting into the icy waters below. A man out for a bike ride witnesses the ordeal and calls 911. An FDNY rescue boat is dispatched. Rescuers pull Aby from the water and begin CPR, and use a defibrillator. Although the rescuers were quick, the water was too cold, and Aby is experiencing severe hypothermia, and is non-responsive.

Aby is rushed to a hospital where they begin the process of warming her up. Eventually she is placed in a room and her family arrives to find her in a coma. Over time, some family members stop visiting, her mother and father fighting over her in the room all the while. 

??? warning "Spoiler"
    Players can unlock the "official" ending of Aby waking up in the hospital room by 100% ing the game. Everyone else will get the typical "roll credits" by defeating the main boss. 

!!! info
    This storyline can breach into the game world at points, for example flashes of light from emergency vehicles, sirens, voices, temperature.

### Settings

- MANHATTAN BRIDGE - WINTER - LATE AFTERNOON
- NEW YORK-PRESBYTERIAN LOWER MANHATTAN HOSPITAL

### Hypothermia

- Shivering
- Slurred speech & mumbling
- Weak pulse
- Clumsiness / poor coordination
- Tired / low energy
- Confusion / memory loss
- Loss of consciousness

### Reference Images

- [Icy Manhattan Bridge Waters](https://www.flickr.com/photos/144398359@N04/38745589261/)
- [FDNY Water rescue boat](https://cdn.firehouse.com/files/base/cygnus/fhc/image/2013/10/960w/fdny-nneeww-pix_11218261.jpg)

## Backstory

### Father

Aby has a verbally abusive father. This has lead to many of her issues. Thankfully, as she got older, she was able to escape his grasp.